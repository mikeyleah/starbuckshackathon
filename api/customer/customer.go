package customer

import (
	"database/sql"
	"encoding/json"
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/julienschmidt/httprouter"
	"purplewifi.com/api/apierrors"
)

const CustomerPrameter = "customerId"

type CustomerRepository struct {
	Database *sqlx.DB
}

type Customer struct {
	CustomerId  uint64         `db:"customer_id" json:"customerId"`
	FirstName   string         `db:"first_name" json:"firstName"`
	LastName    string         `db:"last_name" json:"lastName"`
	DateOfBirth sql.NullString `db:"date_of_birth" json:"dateOfBirth"`
	FbId        sql.NullString `db:"fb_id" json:"fbId"`
	ClientMac   sql.NullString `db:"client_mac" json:"clientMac"`
	UserName    string         `db:"user_name" json:"userName"`
	Password    string         `db:"password" json:"password"`
}

type CustomerLegue struct {
	Position      uint8  `db:"position" json:"position"`
	CustomerId    uint64 `db:"customer_id" json:"customerId"`
	FirstName     string `db:"first_name" json:"firstName"`
	LastName      string `db:"last_name" json:"lastName"`
	TotalCupsUses string `db:"total_cup_uses" json:"totalCupUses"`
}

func (customerRepository CustomerRepository) GetCustomer(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	customerId := ps.ByName(CustomerPrameter)

	if customerId == "" {
		apierrors.HttpError(w, "No valid store id", apierrors.CodeBadRequest)
		return
	}
	customer := Customer{}

	err := customerRepository.Database.Get(&customer, "SELECT * FROM customer WHERE customer_id=?", customerId)
	switch {
	case err == sql.ErrNoRows:
		apierrors.HttpError(w, "Could not find customer", apierrors.CodeNotFound)
		return
	case err != nil:
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
		return
	}

	response, err := json.Marshal(customer)
	if err != nil {
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	w.Write(response)
}

func (customerRepository CustomerRepository) GetTable(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	var output []CustomerLegue

	rows, err := customerRepository.Database.Queryx("SELECT customer.customer_id, customer.first_name, customer.last_name, COUNT(0) AS total_cup_uses FROM starbucks.purchase JOIN starbucks.cup ON cup.cup_id = purchase.cup_id JOIN starbucks.customer ON customer.customer_id = cup.customer_id GROUP BY customer.customer_id, customer.first_name, customer.last_name ORDER BY total_cup_uses desc;")
	count := uint8(1)
	for rows.Next() {
		customer := CustomerLegue{}
		err := rows.StructScan(&customer)
		customer.Position = count
		count++
		if err != nil {
			apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
			return
		}
		output = append(output, customer)
	}

	if len(output) == 0 {
		w.Write([]byte("[]"))
		return
	}

	response, err := json.Marshal(output)
	if err != nil {
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	w.Write(response)
}

func (customerRepository CustomerRepository) AuthCustomer(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if err := r.ParseForm(); err != nil {
		apierrors.HttpError(w, "Could not parse form", apierrors.CodeBadRequest)
		return
	}

	username := r.FormValue("username")
	password := r.FormValue("password")

	customer := Customer{}

	err := customerRepository.Database.Get(&customer, "SELECT * FROM customer WHERE user_name=? AND password=?", username, password)
	switch {
	case err == sql.ErrNoRows:
		apierrors.HttpError(w, "Could not find customer", apierrors.CodeNotFound)
		return
	case err != nil:
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
		return
	}

	response, err := json.Marshal(customer)
	if err != nil {
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	w.Write(response)
}
