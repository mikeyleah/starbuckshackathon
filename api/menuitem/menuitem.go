package menuitem

import (
	"database/sql"
	"encoding/json"
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/julienschmidt/httprouter"
	"purplewifi.com/api/apierrors"
)

const MenuItemParameter = "menuItemId"

type MenuItemRepository struct {
	Database *sqlx.DB
}

type MenuItem struct {
	MenuItemId          uint64  `db:"menu_item_id" json:"menuItemId"`
	Name                string  `db:"name" json:"name"`
	SustainabilityScore float64 `db:"sustainability_score" json:"sustainabilityScore"`
}

func (menuItemRepository MenuItemRepository) GetMenuItems(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	var output []MenuItem

	rows, err := menuItemRepository.Database.Queryx("SELECT * FROM store")
	for rows.Next() {
		menuItem := MenuItem{}
		err := rows.StructScan(&menuItem)
		if err != nil {
			apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
			return
		}
		output = append(output, menuItem)
	}

	response, err := json.Marshal(output)
	if err != nil {
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	w.Write(response)
}

func (menuItemRepository MenuItemRepository) GetMenuItem(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	menuItemId := ps.ByName(MenuItemParameter)

	if menuItemId == "" {
		apierrors.HttpError(w, "No valid menu item id", apierrors.CodeBadRequest)
		return
	}
	menuItem := MenuItem{}

	err := menuItemRepository.Database.Get(&menuItem, "SELECT * FROM menu_item WHERE menu_item_id=?", menuItemId)
	switch {
	case err == sql.ErrNoRows:
		apierrors.HttpError(w, "Could not find menu item", apierrors.CodeNotFound)
		return
	case err != nil:
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
		return
	}

	response, err := json.Marshal(menuItem)
	if err != nil {
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	w.Write(response)
}
