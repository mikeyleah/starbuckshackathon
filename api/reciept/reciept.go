package reciept

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/julienschmidt/httprouter"
	"purplewifi.com/api/apierrors"
)

type RecieptRepository struct {
	Database *sqlx.DB
}

type Reciept struct {
	RecieptId  uint64     `db:"reciept_id" json:"recieptId"`
	CustomerId uint64     `db:"customer_id" json:"customerId"`
	StoreId    uint64     `db:"store_id" json:"storeId"`
	Purchases  []Purchase `db:"-" json:"purchases"`
}

type Purchase struct {
	MenuItemId uint64 `db:"menu_item_id" json:"menuItemId"`
	CupId      uint64 `db:"cup_id" json:"cupId"`
}

func (recieptRepository RecieptRepository) PutPurchase(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var receipt Reciept

	decoder := json.NewDecoder(r.Body)

	err := decoder.Decode(&receipt)
	if err != nil {
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	transaction, err := recieptRepository.Database.Beginx()

	if err != nil {
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	result, err := transaction.NamedExec(`INSERT INTO receipt (customer_id, store_id) VALUES (:customerId, :storeId)`,
		map[string]interface{}{
			"customerId": receipt.CustomerId,
			"storeId":    receipt.StoreId,
		})

	if err != nil {
		transaction.Rollback()
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	id, err := result.LastInsertId()

	log.Printf("%d\n", id)

	if err != nil {
		transaction.Rollback()
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	for _, purchase := range receipt.Purchases {
		if purchase.CupId > 0 {
			_, err = transaction.NamedExec(`INSERT INTO purchase (receipt_id, menu_item_id, cup_id) VALUES (:receiptId, :menuItemId, :cupId)`,
				map[string]interface{}{
					"receiptId":  id,
					"menuItemId": purchase.MenuItemId,
					"cupId":      purchase.CupId,
				})
		} else {
			_, err = transaction.NamedExec(`INSERT INTO purchase (receipt_id, menu_item_id) VALUES (:receiptId, :menuItemId)`,
				map[string]interface{}{
					"receiptId":  id,
					"menuItemId": purchase.MenuItemId,
				})
		}

		if err != nil {
			transaction.Rollback()
			apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
		}
	}

	transaction.Commit()
}
