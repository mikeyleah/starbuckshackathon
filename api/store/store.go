package store

import (
	"database/sql"
	"encoding/json"
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/julienschmidt/httprouter"
	"purplewifi.com/api/apierrors"
)

const StoreParameter = "storeId"

type StoreRepository struct {
	Database *sqlx.DB
}

type Store struct {
	StoreID     uint64         `db:"store_id" json:"storeId"`
	StoreNumber string         `db:"store_number" json:"storeNumber"`
	Slug        string         `db:"slug" json:"slug"`
	Name        string         `db:"name" json:"name"`
	Brand       string         `db:"brand" json:"brand"`
	Address1    string         `db:"address_1" json:"address1"`
	City        string         `db:"city" json:"city"`
	State       string         `db:"state" json:"state"`
	Country     string         `db:"country" json:"country"`
	PostalCode  string         `db:"postal_code" json:"postalCode"`
	PhoneNumber sql.NullString `db:"phone_number" json:"phoneNumber"`
	Latitude    float64        `db:"latitude" json:"latitude"`
	Longitude   float64        `db:"longitude" json:"longitude"`
	Features    string         `db:"features" json:"features"`
	Timezone    string         `db:"timezone" json:"timezone"`
}

func (storeRepository StoreRepository) GetStore(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	storeId := ps.ByName(StoreParameter)

	if storeId == "" {
		apierrors.HttpError(w, "No valid store id", apierrors.CodeBadRequest)
		return
	}
	store := Store{}

	err := storeRepository.Database.Get(&store, "SELECT * FROM store WHERE store_id=?", storeId)
	switch {
	case err == sql.ErrNoRows:
		apierrors.HttpError(w, "Could not find store", apierrors.CodeNotFound)
		return
	case err != nil:
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
		return
	}

	response, err := json.Marshal(store)
	if err != nil {
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	w.Write(response)
}

func (storeRepository StoreRepository) GetStores(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	var output []Store

	rows, err := storeRepository.Database.Queryx("SELECT * FROM store")
	for rows.Next() {
		store := Store{}
		err := rows.StructScan(&store)
		if err != nil {
			apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
			return
		}
		output = append(output, store)
	}

	response, err := json.Marshal(output)
	if err != nil {
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	w.Write(response)
}
