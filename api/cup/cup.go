package cup

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/julienschmidt/httprouter"
	"purplewifi.com/api/apierrors"
)

const CustomerPrameter = "customerId"

const nfcPrameter = "nfcID"

type CupRepository struct {
	Database *sqlx.DB
}

type Cup struct {
	CupId          uint64 `db:"cup_id" json:"cupId"`
	CustomerId     uint64 `db:"customer_id" json:"customerId"`
	NfcId          string `db:"nfc_id" json:"nfcId"`
	RequiredReuses uint64 `db:"required_reuses" json:"requiredReuses"`
}

func (cupRepository CupRepository) PutCup(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var cup Cup

	decoder := json.NewDecoder(r.Body)

	err := decoder.Decode(&cup)
	log.Print(cup)
	if err != nil {
		log.Printf("4: %s\n", err.Error())
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	_, err = cupRepository.Database.Exec(`UPDATE cup SET nfc_id = '' WHERE nfc_id = ?`, cup.NfcId)
	if err != nil {
		log.Printf("3: %s\n", err.Error())
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	_, err = cupRepository.Database.NamedExec(`INSERT INTO cup (customer_id, nfc_id, required_reuses) VALUES (:customerId, :nfcId, :requiredReuses)`,
		map[string]interface{}{
			"customerId":     cup.CustomerId,
			"nfcId":          cup.NfcId,
			"requiredReuses": cup.RequiredReuses,
		})

	if err != nil {
		log.Printf("2: %s\n", err.Error())
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	return
}

func (cupRepository CupRepository) GetCustomerCups(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	customerId := ps.ByName(CustomerPrameter)

	if customerId == "" {
		apierrors.HttpError(w, "No valid customer id", apierrors.CodeBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	var output []Cup

	rows, _ := cupRepository.Database.Queryx("SELECT * FROM cup WHERE customer_id = ? ORDER BY cup_id DESC", customerId)
	for rows.Next() {
		cup := Cup{}
		err := rows.StructScan(&cup)
		if err != nil {
			apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
			return
		}
		output = append(output, cup)
	}

	response, err := json.Marshal(output)
	if err != nil {
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	w.Write(response)
}

func (cupRepository CupRepository) GetCup(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	nfcID := ps.ByName(nfcPrameter)

	if nfcID == "" {
		apierrors.HttpError(w, "No valid store id", apierrors.CodeBadRequest)
		return
	}
	cup := Cup{}

	err := cupRepository.Database.Get(&cup, "SELECT * FROM cup WHERE nfc_id=?", nfcID)
	switch {
	case err == sql.ErrNoRows:
		apierrors.HttpError(w, "Could not find cup", apierrors.CodeNotFound)
		return
	case err != nil:
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
		return
	}

	response, err := json.Marshal(cup)
	if err != nil {
		apierrors.HttpError(w, err.Error(), apierrors.CodeInternalError)
	}

	w.Write(response)
}
