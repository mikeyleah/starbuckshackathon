package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/julienschmidt/httprouter"
	"purplewifi.com/api/cup"
	"purplewifi.com/api/customer"
	"purplewifi.com/api/menuitem"
	"purplewifi.com/api/reciept"
	"purplewifi.com/api/store"
)

var (
	debug         = flag.Bool("debug", false, "enable debugging")
	password      = flag.String("password", "", "the database password")
	port     *int = flag.Int("port", 3306, "the database port")
	server        = flag.String("server", "", "the database server")
	user          = flag.String("user", "", "the database user")
	database      = flag.String("database", "", "the database")
)

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "Welcome!\n")
}

func main() {
	flag.Parse()

	if *debug {
		fmt.Printf(" password:%s\n", *password)
		fmt.Printf(" port:%d\n", *port)
		fmt.Printf(" server:%s\n", *server)
		fmt.Printf(" user:%s\n", *user)
		fmt.Printf(" database:%s\n", *database)
	}

	router := httprouter.New()
	router.GET("/", Index)

	connString := fmt.Sprintf("%s:%s@(%s:%d)/%s?parseTime=true", *user, *password, *server, *port, *database)
	//"user:password@(starbucks-db.datascience.lan:3306)/starbucks?parseTime=true"
	db, err := sqlx.Connect("mysql", connString)
	if err != nil {
		log.Fatalln(err)
	}

	storeRepository := store.StoreRepository{Database: db}
	menuItemRepository := menuitem.MenuItemRepository{Database: db}
	customerRepository := customer.CustomerRepository{Database: db}
	recieptRepository := reciept.RecieptRepository{Database: db}
	cupRepository := cup.CupRepository{Database: db}
	router.GET("/store/:storeId", storeRepository.GetStore)
	router.GET("/store/", storeRepository.GetStores)
	router.GET("/menuItem/:menuItemId", menuItemRepository.GetMenuItem)
	router.GET("/menuItem/", menuItemRepository.GetMenuItems)
	router.POST("/auth/", customerRepository.AuthCustomer)
	router.GET("/customer/:customerId", customerRepository.GetCustomer)
	router.GET("/table/", customerRepository.GetTable)
	router.PUT("/order/", recieptRepository.PutPurchase)
	router.PUT("/cup/", cupRepository.PutCup)
	router.GET("/cup/:nfcID", cupRepository.GetCup)
	router.GET("/cup-customer/:customerId", cupRepository.GetCustomerCups)

	log.Fatal(http.ListenAndServe(":8080", router))
}
