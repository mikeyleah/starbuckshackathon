package apierrors

import (
	"fmt"
	"log"
	"net/http"
)

const CodeNotFound = http.StatusNotFound
const CodeBadRequest = http.StatusBadRequest
const CodeInternalError = http.StatusInternalServerError

const errorJSONFormat = `{"error": true, "message": "%s"}`

func getErrorJSON(message string) []byte {
	return []byte(fmt.Sprintf(errorJSONFormat, message))
}

func HttpError(w http.ResponseWriter, error string, code int) {
	//w.Header().Set("Content-Type", "application/json")
	//w.Header().Set("X-Content-Type-Options", "nosniff")
	//w.WriteHeader(code)
	log.Printf("got error: %s", error)
	w.Write(getErrorJSON(error))
}
