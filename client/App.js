/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ScrollView} from 'react-native';
import Login from './components/Login';
import CupRegister from './components/CupRegister';
import LegueTable from './components/LegueTable';
import { MAIN_APP } from './components/Styles';


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {
  state = {
    Customer: {
      customerId: 0,
      firstName: '',
      lastName: ''
    },
    isLoggedIn: false
  }

  render() {

    if (this.state.isLoggedIn) 
      return (
        <ScrollView styles={MAIN_APP.main} contentContainerStyle={MAIN_APP.mainContainerStyle}>
          <CupRegister customer={this.state.Customer} />
          <LegueTable  customer={this.state.Customer} />
        </ScrollView>
      );
    else 
      return (
        <Login 
          onLoginPress={(customer) => { this.setState({Customer: customer}); this.setState({isLoggedIn: true}); console.log("sent state: ", this.state.Customer)}}
        />
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
