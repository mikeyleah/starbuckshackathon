import React, { Component } from 'react';
import {
    StyleSheet,
} from 'react-native';

export const BUTTON_STYLES = StyleSheet.create({
    submit:{
        marginRight:40,
        marginLeft:40,
        marginTop:10,
        paddingTop:20,
        paddingBottom:20,
        backgroundColor:'#00704A',
        borderRadius:100
      },
      submitText:{
          color:'#fff',
          textAlign:'center',
      }
});

export const LAYOUT = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'stretch',
        marginTop: 0,
        marginBottom: 0,
        marginLeft: 0,
        marginRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0,
    },
    topContainer: {
        flex: 1,
        flexShrink: 0,
        marginLeft: 0,
        paddingRight: 0,
        paddingTop: 8,
        backgroundColor: '#F7F7F7',
        shadowColor: "#000",
        shadowOffset: {
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        minHeight: 67
    },
    contentContainer: {
        flex: 5
    },
    bottomContainer: {
        flex: 1,
        marginBottom: 30
    }
});

export const LOGIN_SCREEN = StyleSheet.create({
    image: {
        flex: 1,
        width: 40,
        height: 40,
        alignSelf: 'flex-end',
        overflow: 'visible',
        marginRight: 10,
        resizeMode: 'contain'
    },
    text: {
        flex: 1,
        fontSize: 27,
        marginLeft: 10,
    },
    loginContainer: {
        marginTop: 20,
        paddingLeft:10,
        paddingRight:10
    },
    loginButton: {
        width: 120,
        alignSelf: 'flex-end',
        overflow: 'visible',
        marginRight: 10,
    }
});

export const INPUT_STYLE = StyleSheet.create({
    input: {
        borderBottomWidth: 1,
        borderColor: '#ABABAB'
    }
});

export const MAIN_APP = StyleSheet.create({
    main: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 0,
        marginBottom: 0,
        marginLeft: 0,
        marginRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0,
    },
    mainContainerStyle: {
        justifyContent: 'flex-start',
        alignItems: 'stretch',
    }
});

export const REGISTER_CUP = StyleSheet.create({
    head: {
        flex: 1,
        flexShrink: 0,
        marginLeft: 0,
        paddingRight: 0,
        paddingTop: 8,
        backgroundColor: '#F7F7F7',
        shadowColor: "#000",
        shadowOffset: {
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        minHeight: 67,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    headText: {
        flex: 4,
        fontSize: 27,
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
    },
    headImage: {
        flex: 4,
        fontSize: 27,
        marginRight: 10,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 10,
        resizeMode: 'contain'
    },
    tagStyle: {
        fontSize: 17,
        textAlign: 'center',
        marginTop: 20,
        marginBottom: 20
    }
});
