import React, { Component } from 'react';
import {
    Text,
    TextInput,
    View,
    ScrollView,
    Button,
    Image,
    TouchableHighlight,
    ActivityIndicator,
    StyleSheet
} from 'react-native';

import { BUTTON_STYLES, LAYOUT, LOGIN_SCREEN, INPUT_STYLE } from './Styles';

export default class Login extends Component {
    state = {
        username: '',
        password: '',
        Response: {
          customerId: 0,
          firstName: '',
          lastName: ''
        },
        isLoggingIn: false,
        message: '',
    }
    
    _userLogin = () => {
        console.log('doing login');
        this.setState({ isLoggingIn: true, message: ''});

        let params = {
            username: this.state.username.toLowerCase(),
            password: this.state.password.toLowerCase(),
        };


        var formBody = [];
        for (var property in params) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(params[property]);
          formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        fetch("http://40.117.46.107:8080/auth/", {
            method: "POST", 
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: formBody
            })
            .then((response) => response.json())
            .then((response) => {
                console.log(response);
                if (response.customerId > 0){
                    this.setState({ Response: response})
                } else {
                    this.setState({ message: "user not found" });
				    this.setState({ isLoggingIn: false });
                }
            })
            .then(() => {
                this.setState({ isLoggingIn: false })
                if (this.state.Response.customerId > 0) this.props.onLoginPress(this.state.Response);
            })
            .catch(err => {
				this.setState({ message: "Network error" });
				this.setState({ isLoggingIn: false })
			});
    }

    render() {
        return (
            

            <View style={LAYOUT.mainContainer}>
              
                <View style={styles.header}>
                    <Text style={styles.headerText}>Sign in to Rewards</Text>
                    <Image source={require('./../images/starbucksLogo.png')} style={styles.headerLogo}/>
                </View>


                <View style={[LAYOUT.contentContainer, LOGIN_SCREEN.loginContainer]}>
                    <TextInput
                        ref={component => this._username = component}
                        placeholder='Username' 
                        onChangeText={(username) => this.setState({username})}
                        style={INPUT_STYLE.input}
                        underlineColorAndroid= 'transparent'
                        autoCapitalize = 'none'
                    />
                    <TextInput
                        ref={component => this._password = component}
                        placeholder='Password' 
                        onChangeText={(password) => this.setState({password})}
                        style={INPUT_STYLE.input}
                        underlineColorAndroid= 'transparent'
                        autoCapitalize = 'none'
                        secureTextEntry={true}
                    />
                    {!!this.state.message && (
                        <Text
                            style={{fontSize: 14, color: 'red', padding: 5}}>
                            {this.state.message}
                        </Text>
                    )}
                    {this.state.isLoggingIn && <ActivityIndicator />}

                    <View style={styles.recoveryContainer}>
                        <Text style={styles.recoveryText}>Forgotten Password?</Text>
                        <Text style={styles.recoveryText}>Forgotten Username?</Text>
                    </View>
                </View>
                

                <View style={styles.signInContainer}>
                    <TouchableHighlight onPress={this._userLogin} style={styles.button}>
                        <Text style={styles.buttonText}>Sign In</Text>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    recoveryContainer: {
        marginTop: 20
    },
    recoveryText: {
        color: '#00704A',
        marginBottom: 8
    },
    header: {
        padding: 25,
        elevation: 4,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#f7f7f7'
    },
    headerLogo: {
        width: 60,
        height: 60
    },
    headerText: {
        fontSize: 30,
        lineHeight: 55
    },
    button: {
        backgroundColor: '#00704A',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 50,
        paddingRight: 50,
        borderRadius: 25,
        alignSelf: 'center',
        marginTop: -20
    },
    buttonText: {
        color: '#f8f8f8',
        fontSize: 16
    },
    signInContainer: {
        alignSelf: 'flex-end',
        margin: 25
    }
})