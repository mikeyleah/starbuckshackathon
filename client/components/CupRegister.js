import React, { Component } from 'react';
import {
    Platform,
    ScrollView,
    Text,
    TextInput,
    View,
    Button,
    ActivityIndicator,
    TouchableOpacity,
    StyleSheet,
    Image,
    Alert
} from 'react-native';

import {REGISTER_CUP, LOGIN_SCREEN, BUTTON_STYLES} from './Styles';
import NfcManager, {ByteParser, Ndef, NdefParser} from 'react-native-nfc-manager'

const styles = StyleSheet.create({
    header: {
        padding: 25,
        elevation: 4,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#f7f7f7'
    },
    headerLogo: {
        width: 60,
        height: 60
    },
    headerText: {
        fontSize: 30,
        lineHeight: 55
    },
    heroImage: {
        width: '100%',
        height: 200
    },
    button: {
        backgroundColor: '#00704A',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 50,
        paddingRight: 50,
        borderRadius: 25,
        alignSelf: 'center',
        marginTop: -20
    },
    buttonText: {
        color: '#f8f8f8',
        fontSize: 16
    },
    cupContainer: {
        padding: 20,
        backgroundColor: '#fff',
        marginTop: 0,
        borderBottomWidth: 4,
        borderBottomColor: '#00704A'
    },
    cupContainerText: {
        fontSize: 18,
        alignSelf: 'center'
    }
})

export default class CupRegister extends Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            Customer: props.customer,
            supported: true,
            tag: {},

            scanText: 'Scan Cup'
        }
    }

    componentDidMount() {
        NfcManager.isSupported()
            .then(supported => {
                this.setState({ supported });
                if (supported) {
                    this._startNfc();
                }
            })
    }

    render() {
        return (
            <View style={{backgroundColor: '#fff'}}>
                <View style={styles.header}>
                    <Text style={styles.headerText}>Register Your Cup</Text>
                    <Image source={require('./../images/starbucksLogo.png')} style={styles.headerLogo}/>
                </View>
                <Image source={require('./../images/RegisterYourCup.jpeg')} style={styles.heroImage}/>
                
                <TouchableOpacity style={styles.button} onPress={() => {
                    this._startDetection()
                }}>
                    <Text style={styles.buttonText}>{this.state.scanText}</Text>
                </TouchableOpacity>

                <View style={styles.cupContainer}>
                    <Text style={styles.cupContainerText}>Your Cup ID: {this.state.tag.id} </Text>
                </View>
            </View>
        );
    }

    _startNfc() {
        NfcManager.start({
            onSessionClosedIOS: () => {
                console.log('ios session closed');
            }
        })
            .then(result => {
                console.log("start OK", result);
            })
            .catch(error => {
                console.warn("start fail", error);
                this.setState({supported: false});
            })
    
        if (Platform.OS === 'android') {
            NfcManager.getLaunchTagEvent()
                .then(tag => {
                    console.log('launch tag', tag);
                    if (tag) {
                        this.setState({ tag });
                    }
                })
                .catch(err => {
                    console.log(err);
                })
            NfcManager.isEnabled()
                .then(enabled => {
                    this.setState({ enabled });
                })
                .catch(err => {
                    console.log(err);
                })
            NfcManager.onStateChanged(
                event => {
                    if (event.state === 'on') {
                        this.setState({enabled: true});
                    } else if (event.state === 'off') {
                        this.setState({enabled: false});
                    } else if (event.state === 'turning_on') {
                        // do whatever you want
                    } else if (event.state === 'turning_off') {
                        // do whatever you want
                    }
                }
            )
                .then(sub => {
                    this._stateChangedSubscription = sub; 
                    // remember to call this._stateChangedSubscription.remove()
                    // when you don't want to listen to this anymore
                })
                .catch(err => {
                    console.warn(err);
                })
        }
    }

    _startDetection = () => {
        this.setState({scanText: 'Scanning...'})
        console.log("starting detection");
        NfcManager.registerTagEvent(this._onTagDiscovered)
            .then(result => {
                console.log('registerTagEvent OK', result)
            })
            .catch(error => {
                console.warn('registerTagEvent fail', error)
            })
    }

    _onTagDiscovered = tag => {
        console.log('Tag Discovered', tag);
        this.setState({ tag });
        this.setState({scanText: 'Scan Cup'})

        fetch("http://40.117.46.107:8080/cup/", {
            method: "PUT",
            body: JSON.stringify({
                customerId: this.state.Customer.customerId,
                nfcId: tag.id,
                requiredReuses: 10,
                }),
            })
            .then(response => {
                if (response.status == 200) {
                    this._stopDetection();
                }
            })
            .catch(err => {
                console.log(err);
            });

            this._stopDetection();
    }

    _stopDetection = () => {
        NfcManager.unregisterTagEvent()
            .then(result => {
                console.log('unregisterTagEvent OK', result)
            })
            .catch(error => {
                console.warn('unregisterTagEvent fail', error)
            })
    }
}
