import React, { Component } from 'react';
import {
    Platform,
    ScrollView,
    Text,
    TextInput,
    View,
    Button,
    ActivityIndicator,
    StyleSheet
} from 'react-native';

import { Table, TableWrapper, Row } from 'react-native-table-component';

export default class LegueTable extends Component {
    state = {
        Customer: {
            customerId: 0,
            firstName: '',
            lastName: ''
        },
        LegueTable: [],
    }
    
    constructor(props) {
        super(props);
        this.state = {
            Customer: props.customer,
            LegueTable: [],
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        this._getResults();
        this.interval = setInterval(() => {
            this._getResults();
        }, 5000);
        return (
            <View style={styles.leaderboardContainer}>
                <Text style={styles.leaderboardTitle}>Leaderboard</Text>

                <View style={[styles.tableRow, styles.tableRowHeader]}>
                    <Text style={styles.tableRowItem}>Position</Text>
                    <Text style={styles.tableRowItem}>Name</Text>
                    <Text style={styles.tableRowItem}>Usage</Text>
                </View>

                {this.state.LegueTable.map((customer, index) => {

                    return (
                        <View style={[styles.tableRow, index%2 && {backgroundColor: '#F7F6E7'}]} key={index}>
                            <Text style={[styles.tableRowItem, styles.textBlack]}>{customer.position}</Text>
                            <Text style={[styles.tableRowItem, styles.textBlack]}>{customer.firstName} {customer.lastName}</Text>
                            <Text style={[styles.tableRowItem, styles.textBlack]}>{customer.totalCupUses}</Text>
                        </View>
                    )
                })}
            </View>
        );
    }

    _getResults(){
        fetch('http://40.117.46.107:8080/table/', {method: "GET"})
         .then((response) => response.json())
         .then((response) =>
         {
           this.setState({ LegueTable: response})
         })
         .catch((error) => {
             console.error(error);
         });
    }


}


const styles = StyleSheet.create({
    header: {
        padding: 25,
        elevation: 4,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#f7f7f7'
    },
    headerLogo: {
        width: 60,
        height: 60
    },
    headerText: {
        fontSize: 30,
        lineHeight: 55
    },
    leaderboardContainer: {
        margin: 25
    },
    leaderboardTitle: {
        fontSize: 18,
        marginBottom: 10
    },
    tableRow: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    tableRowHeader: {
        backgroundColor: '#00704A',
    },
    tableRowItem: {
        color: '#fff',
        padding: 10,
        alignSelf: 'flex-start'
    },
    textBlack: {
        color: '#27251F'
    }
})