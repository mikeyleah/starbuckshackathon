#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 20:02:03 2019

@author: mat
"""
import glob
import requests

import numpy as np
import pandas as pd
import geopandas as gpd

from shapely.geometry import Point

import matplotlib.pyplot as plt


def build_sampling_grid(gdf, index, x_points, y_points):
    """Determines an x by y sampling grid, for values which lie within a
    shapefile"""
    bounds = gdf.bounds.loc[index]

    xx, yy = np.meshgrid(
        np.linspace(bounds["minx"], bounds["maxx"], x_points),
        np.linspace(bounds["miny"], bounds["maxy"], y_points),
    )

    xc = xx.flatten()
    yc = yy.flatten()

    points = gpd.GeoSeries([Point(x, y) for x, y in zip(xc, yc)])
    mask = points.within(gdf.geometry.loc[index])

    return np.hstack((xc[mask].reshape(-1, 1), yc[mask].reshape(-1, 1)))


def plot_sample_points(gdf, index, sample_points):
    """Plots sampling grid on a geometry object"""
    fig, ax = plt.subplots(1)

    gdf.geometry.plot(ax=ax)
    ax.scatter(sample_points[:, 0], sample_points[:, 1], color="red", marker=".")

    return fig


def download_sample_data(lat, lng):
    """Downloads data from the Starbucks store finder for the specified
    latitude and longitude pair"""

    def process_store(store):
        return pd.Series(
            {
                "store_id": store["id"],
                "store_number": store["storeNumber"],
                "slug": store["slug"],
                "name": store["name"],
                "brand": store["brandName"],
                "address_1": store["address"]["streetAddressLine1"],
                "city": store["address"]["city"],
                "state": store["address"]["countrySubdivisionCode"],
                "country": store["address"]["countryCode"],
                "postal_code": store["address"]["postalCode"],
                "phone_number": store["phoneNumber"],
                "latitude": store["coordinates"]["latitude"],
                "longitude": store["coordinates"]["longitude"],
                "features": store["features"],
                "timezone": store["timeZoneInfo"]["olsonTimeZoneId"],
            }
        )

    headers = {
        "Accept": "application/json",
        "Accept-Language": "en-GB,en;q=0.5",
        "X-Requested-With": "XMLHttpRequest",
        "Connection": "keep-alive",
        "Pragma": "no-cache",
        "Cache-Control": "no-cache",
        "TE": "Trailers",
    }

    params = (("lat", lat), ("lng", lng))

    response = requests.get(
        "https://www.starbucks.com/bff/locations", headers=headers, params=params
    )

    stores = [process_store(store) for store in response.json()["stores"]]
    return pd.concat(stores, axis=1).T


def download_all_stores(sample_points):
    """Downloads and de-duplicates data for all latitide longitde pairs
    supplied"""
    all_store_data = []
    for lng, lat in sample_points:
        all_store_data.append(download_sample_data(lat, lng))

    return pd.concat(all_store_data)


def aggregate_geo(path='./cities/*.geojson'):
    files = glob.glob(path)
    out = []
    for file in files:
        row = gpd.GeoDataFrame.from_file(file)
        row['filename'] = file
        out.append(row)
    all_geometries = pd.concat(out)
    all_geometries.reset_index(drop=True, inplace=True)
    return all_geometries


def main():
    all_geometries = aggregate_geo()

    all_data = []
    for i, location in all_geometries.iterrows():
        sample_points = build_sampling_grid(all_geometries, i, 9, 12)
        all_data.append(download_all_stores(sample_points))
        print(f'Processed data for: {location["filename"]}')
    store_data = pd.concat(all_data)
    store_data.drop_duplicates(subset="store_id", inplace=True)
    store_data.to_csv("./Starbucks_Stores.csv", index=False)

    try:
        import json
        from purple.sql import mysql_connect
        cxn = mysql_connect("starbucks-purple", "rQPtN9dy2Xjt", "40.117.46.107:3306", "starbucks")
        store_data['features'] = store_data['features'].apply(json.dumps)
        store_data.to_sql('store', cxn, if_exists='append', index=False)
    except ImportError:
        print('Unable to upload data to SQL')
        pass

if __name__ == "__main__":
    main()
