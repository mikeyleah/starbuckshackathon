#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  4 21:00:52 2019

@author: mat
"""
import uuid
import random
import string
from datetime import timedelta

import numpy as np
import pandas as pd
import geopandas as gpd

from shapely.geometry import Point
from shapely.ops import nearest_points

from purple.sql import mysql_connect

cycle_data = pd.read_csv("./bikeshare/trip.csv", error_bad_lines=False)
cycle_data["starttime"] = pd.to_datetime(
    cycle_data["starttime"], format="%m/%d/%Y %H:%M"
)
cycle_data["stoptime"] = pd.to_datetime(cycle_data["stoptime"], format="%m/%d/%Y %H:%M")

cycle_data["starttime"] = cycle_data["starttime"] + timedelta(days=1009)
cycle_data["stoptime"] = cycle_data["stoptime"] + timedelta(days=1009)


station_data = pd.read_csv("./bikeshare/station.csv")

cxn = mysql_connect(
    "user", "password", "starbucks-test-db.datascience.lan:3307", "starbucks"
)


#%%


menu_items = {
    "Caffè Americano": 100,
    "Caffè Latte": 85,
    "Caffè Mocha": 70,
    "Cappucino": 85,
    "Espresso": 120,
    "Flat White": 90,
    "Espresso Con Panna": 85,
    "Chai Latte": 90,
    "Royal English Breakfast Tea": 120,
    "Teavana® Shaken Iced White Tea": 100,
}

menu_items = pd.DataFrame(pd.Series(menu_items))
menu_items.reset_index(inplace=True)
menu_items.reset_index(inplace=True)
menu_items.rename(
    columns={"level_0": "menu_item_id", "index": "name", 0: "sustainability_score"},
    inplace=True,
)
menu_items["menu_item_id"] = menu_items["menu_item_id"] + 1

menu_items.to_sql("menu_item", cxn, if_exists="append", index=False)


#%%
class ColumnSampler(object):
    def __init__(self, series):
        self.value_counts = series.value_counts()

    def sample(self, n=1):
        return np.random.choice(
            self.value_counts.index,
            n,
            p=self.value_counts.values / self.value_counts.values.sum(),
        )


def fillna(df, column):
    sampler = ColumnSampler(df[column])

    nulls = cycle_data[column].isna()
    df.loc[nulls, column] = sampler.sample(nulls.sum())


class NameGenerator(object):
    def __init__(self):
        with open("./random_names/male_firstnames.txt", "r") as f:
            male_names = f.read().split()
        self.male_names = [x.capitalize() for x in male_names]
        with open("./random_names/female_firstnames.txt", "r") as f:
            female_names = f.read().split()
        self.female_names = [x.capitalize() for x in female_names]
        with open("./random_names/surnames.txt", "r") as f:
            surnames = f.read().split()
        self.surnames = [x.capitalize() for x in surnames]

    def name(self, gender):
        if gender == "Other":
            gender = random.choice(("Male", "Female"))
        if gender == "Male":
            return " ".join(
                (random.choice(self.male_names), random.choice(self.surnames))
            )
        elif gender == "Female":
            return " ".join(
                (random.choice(self.female_names), random.choice(self.surnames))
            )


class DOBGenerator(object):
    def __init__(self):
        self.dates = pd.DatetimeIndex(start="2018-01-01", end="2018-12-31", freq="1d")

    def date(self, birthyear):
        date = random.choice(self.dates)
        date = date.strftime("-%m-%d")
        return pd.to_datetime(f"{int(birthyear):d}" + date).date()


def mac_maker():
    if random.randint(1, 101) > 15:
        return "%02x:%02x:%02x:%02x:%02x:%02x" % (
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255),
        )


#%%


fillna(cycle_data, "birthyear")
fillna(cycle_data, "gender")

#%%

dob_maker = DOBGenerator()
name_maker = NameGenerator()


cycle_data["rng"] = [random.randint(1, 101) for x in range(len(cycle_data))]

customer_distinct_columns = ["rng", "birthyear", "gender"]

customers = cycle_data.groupby(customer_distinct_columns).agg({"trip_id": "count"})

customers.reset_index(inplace=True)
customers.sample(frac=1)
customers.reset_index(inplace=True)
customers.rename(
    columns={"index": "customer_id", "trip_id": "visit_count"}, inplace=True
)

customers["customer_id"] = customers["customer_id"] + 1

customers["name"] = customers["gender"].apply(name_maker.name)
customers["date_of_birth"] = customers["birthyear"].apply(dob_maker.date)
customers["client_mac"] = [mac_maker() for x in range(len(customers))]

customers["first_name"] = customers["name"].apply(lambda x: x.split()[0])
customers["last_name"] = customers["name"].apply(lambda x: x.split()[1])
customers["user_name"] = customers["name"].apply(lambda x: x.replace(" ", ".").lower())
customers["password"] = "password"


#%%


to_upload = customers[
    [
        "customer_id",
        "first_name",
        "last_name",
        "user_name",
        "password",
        "date_of_birth",
        "client_mac",
    ]
]

to_upload.to_sql("customer", cxn, if_exists="append", index=False)

#%%


def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(stringLength))


customers["has_cup"] = [random.randint(1, 101) > 68 for x in range(len(customers))]
cups = customers[customers["has_cup"]]
cups.reset_index(drop=True, inplace=True)
cups.reset_index(inplace=True)
cups.rename(columns={"index": "cup_id"}, inplace=True)
cups["cup_id"] = cups["cup_id"] + 1

cups["nfc_id"] = [randomString(stringLength=32) for x in range(len(cups))]
cups["required_reuses"] = [random.randint(4, 11) for x in range(len(cups))]


cups_upload = cups[["cup_id", "customer_id", "nfc_id", "required_reuses"]]
cups_upload.to_sql("cup", cxn, if_exists="append", index=False)

#%%


user_cycle_data = pd.merge(
    cycle_data,
    customers[customer_distinct_columns + ["customer_id", "client_mac"]],
    on=customer_distinct_columns,
)
#%%


stores = pd.read_sql("SELECT * FROM starbucks.store WHERE city = 'Seattle';", cxn)


def nearest(
    row,
    geom_union,
    df1,
    df2,
    geom1_col="geometry",
    geom2_col="geometry",
    src_column=None,
):
    """Find the nearest point and return the corresponding value from specified column."""
    # Find the geometry that is closest
    nearest = df2[geom2_col] == nearest_points(row[geom1_col], geom_union)[1]
    # Get the corresponding value from df2 (matching is based on the geometry)
    value = df2[nearest][src_column].get_values()[0]
    return value


def random_closest_store(station):
    return random.choice(
        store_index[store_index["closest_station"] == station]["store_id"].values
    )


station_data["geometry"] = [Point(x.long, x.lat) for i, x in station_data.iterrows()]
stores["geometry"] = [Point(x.longitude, x.latitude) for i, x in stores.iterrows()]

unary_union = gpd.GeoDataFrame(station_data).unary_union

stores["closest_station"] = stores.apply(
    nearest,
    geom_union=unary_union,
    df1=stores,
    df2=station_data,
    src_column="station_id",
    axis=1,
)

store_index = stores[["closest_station", "store_id"]]


from_journies = user_cycle_data[
    user_cycle_data["from_station_id"].isin(store_index["closest_station"])
]
from_journies["store_id"] = from_journies["from_station_id"].apply(random_closest_store)


#%%


def random_time(start, duration):
    return start + timedelta(seconds=random.randint(duration))


#%%

from_journies["first_seen"] = from_journies["starttime"]
from_journies["last_seen"] = [
    x["first_seen"] + timedelta(seconds=x["tripduration"])
    for i, x in from_journies.iterrows()
]
from_journies["wifi_login"] = [
    x["first_seen"] + timedelta(seconds=random.randint(1, int(x["tripduration"] / 3)))
    for i, x in from_journies.iterrows()
]

from_journies["converted"] = from_journies["usertype"].apply(lambda x: x == "Member")
from_journies.sort_values("first_seen", inplace=True)


#%%
from_journies.replace({None: np.nan}, inplace=True)
wifi_presence = from_journies.dropna(subset=["client_mac"])

wifi_presence.reset_index(drop=True, inplace=True)
wifi_presence.reset_index(inplace=True)
wifi_presence.rename(columns={"index": "wifi_presence_id"}, inplace=True)
wifi_presence["wifi_presence_id"] = wifi_presence["wifi_presence_id"] + 1

wifi_presence_upload = wifi_presence[
    [
        "wifi_presence_id",
        "client_mac",
        "store_id",
        "first_seen",
        "last_seen",
        "converted",
    ]
]
wifi_presence_upload.to_sql("wifi_presence", cxn, if_exists="append", index=False)

#%%

wifi_visit = wifi_presence[wifi_presence["usertype"] == "Member"]
wifi_visit.dropna(subset=["client_mac"], inplace=True)
wifi_visit.sort_values("wifi_presence_id", inplace=True)
wifi_visit.reset_index(drop=True, inplace=True)
wifi_visit.reset_index(inplace=True)
wifi_visit.rename(columns={"index": "wifi_visit_id"}, inplace=True)
wifi_visit["wifi_visit_id"] = wifi_visit["wifi_visit_id"] + 1
wifi_visit["bytes_downloaded"] = (
    wifi_visit["store_id"] * wifi_visit["customer_id"]
).apply(lambda x: int(random.randint(0, x)))
wifi_visit["bytes_uploaded"] = (
    wifi_visit["store_id"] * wifi_visit["customer_id"]
).apply(lambda x: int(random.randint(0, x) / 10))


wifi_visit_upload = wifi_visit[
    [
        "wifi_visit_id",
        "wifi_presence_id",
        "store_id",
        "customer_id",
        "first_seen",
        "last_seen",
        "wifi_login",
    ]
]
wifi_visit_upload.to_sql("wifi_visit", cxn, if_exists="append", index=False)

#%%

wifi_visit["reciepts"] = np.random.choice(
    [0, 1, 2, 3], len(wifi_visit), p=[0.2, 0.65, 0.14, 0.01]
)

#%%
def roundTime(dt=None, roundTo=60):
    """Round a datetime object to any time lapse in seconds
   dt : datetime.datetime object, default now.
   roundTo : Closest number of seconds to round to, default 1 minute.
   Author: Thierry Husson 2012 - Use it as you want but don't blame me.
   """
    seconds = 0  # (dt.replace(tzinfo=None) - dt.min).seconds
    rounding = (seconds + roundTo / 2) // roundTo * roundTo
    return dt + timedelta(0, rounding - seconds, -dt.microsecond)


wifi_visit["round_time"] = wifi_visit["first_seen"].apply(roundTime, roundTo=3 * 60)

wifi_visit = pd.merge(
    wifi_visit, cups[["customer_id", "cup_id"]], on="customer_id", how="left"
)

#%%
reciepts = []
purchases = []
visits = []

purchase_id = 1
reciept_id = 1
for i, group in wifi_visit.groupby(["store_id", "round_time"]):
    visit_id = group['wifi_visit_id'].sample(1).values[0]
    for j in range(group["reciepts"].sample(1).values[0]):
        reciept = pd.Series(
            {
                "reciept_id": reciept_id,
                "customer_id": group["customer_id"].sample(1).values[0],
                "store_id": group["store_id"].sample(1).values[0],
                "timestamp": (
                    group["first_seen"].sample(1)
                    + timedelta(seconds=random.randint(90, 450))
                ).values[0],
            }
        )

        visit = pd.Series({'wifi_visit_id': visit_id,
                           'receipt_id': reciept_id})

        reciepts.append(reciept)
        visits.append(visit)
        items = random.randint(len(group)-1, len(group)*2)
        cups = np.append(np.zeros(items), group['cup_id'].values)

        for k in range(items):

            purchase = pd.Series({'purchase_id': purchase_id,
                                  'receipt_id': reciept_id,
                                  'menu_item_id': menu_items['menu_item_id'].sample(1).values[0],
                                  'cup_id': np.random.choice(cups, replace=False)})
            purchases.append(purchase)
            purchase_id += 1


        reciept_id += 1

#%%

reciept_table = pd.concat(reciepts, axis=1).T
purchase_table = pd.concat(purchases, axis=1).T
visit_reciept_table = pd.concat(visits, axis=1).T

#%%
reciept_table['receipt_id'] = pd.to_numeric(reciept_table['reciept_id'])
reciept_table['customer_id'] = pd.to_numeric(reciept_table['customer_id'])
reciept_table['store_id'] = pd.to_numeric(reciept_table['store_id'])

reciept_table['timestamp_created'] = pd.to_datetime(reciept_table['timestamp'])

reciept_table[['customer_id', 'store_id', 'timestamp_created', 'receipt_id']].to_sql('receipt', cxn, if_exists='append', index=False)

#%%
purchase_table.loc[purchase_table['cup_id'] < 0.5, 'cup_id'] = np.nan
purchase_table['purchase_id'] = purchase_table['purchase_id'].astype(int)
purchase_table['receipt_id'] = purchase_table['receipt_id'].astype(int)
purchase_table['menu_item_id'] = purchase_table['menu_item_id'].astype(int)



purchase_table[['purchase_id', 'receipt_id', 'menu_item_id', 'cup_id']].to_sql('purchase', cxn, if_exists='append', index=False)


#%%


visit_reciept_table.to_sql('wifi_visit_receipt', cxn, if_exists='append', index=False)

#%%


cxn2 = mysql_connect(
    "starbucks-purple", "rQPtN9dy2Xjt", "40.117.46.107:3306", "starbucks"
)





