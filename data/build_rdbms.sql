DROP TABLE IF EXISTS `starbucks`.`cup`, `starbucks`.`customer`, `starbucks`.`menu_item`, `starbucks`.`purchase`, `starbucks`.`receipt`, `starbucks`.`store`, `starbucks`.`wifi_presence`, `starbucks`.`wifi_visit`, `starbucks`.`wifi_visit_receipt`;

CREATE TABLE IF NOT EXISTS `starbucks`.`store` (
   `store_id`              INT             NOT NULL
 , `store_number`          VARCHAR(64)     NOT NULL
 , `slug`                  VARCHAR(256)    NOT NULL
 , `name`                  VARCHAR(256)    NOT NULL
 , `brand`                 VARCHAR(64)     NOT NULL
 , `address_1`             VARCHAR(256)    NOT NULL
 , `city`                  VARCHAR(256)    NOT NULL
 , `state`                 VARCHAR(64)     NOT NULL
 , `country`               VARCHAR(64)     NOT NULL
 , `postal_code`           VARCHAR(16)     NOT NULL
 , `phone_number`          VARCHAR(16)     NULL
 , `latitude`              NUMERIC(10, 6)  NOT NULL
 , `longitude`             NUMERIC(10, 6)  NOT NULL
 , `timezone`              VARCHAR(64)     NOT NULL
 , `features`              VARCHAR(1000)   NOT NULL
 , INDEX (`store_id`)
 , PRIMARY KEY (`store_id`)
  );

CREATE TABLE IF NOT EXISTS `starbucks`.`menu_item` (
   `menu_item_id`          INT              NOT NULL AUTO_INCREMENT
 , `name`                  VARCHAR(1000)    NOT NULL
 , `sustainability_score`  NUMERIC(4, 1)    NOT NULL
 , INDEX(`menu_item_id`)
 , PRIMARY KEY(`menu_item_id`)
);

CREATE TABLE IF NOT EXISTS `starbucks`.`customer` (
   `customer_id`           INT              NOT NULL AUTO_INCREMENT
 , `first_name`            VARCHAR(64)      NOT NULL
 , `last_name`             VARCHAR(64)      NOT NULL
 , `user_name`             VARCHAR(128)     NOT NULL
 , `password`              VARCHAR(64)      NOT NULL
 , `date_of_birth`         DATE             NULL
 , `client_mac`            VARCHAR(32)      NULL
 , `fb_id`                 VARCHAR(32)      NULL
 , INDEX(`customer_id`)
 , PRIMARY KEY(`customer_id`)
);

CREATE TABLE IF NOT EXISTS `starbucks`.`cup` (
   `cup_id`                INT              NOT NULL AUTO_INCREMENT
 , `nfc_id`                VARCHAR(64)        NOT NULL
 , `customer_id`           INT              NULL
 , `required_reuses`       INT              NULL
 , INDEX(`cup_id`)
 , PRIMARY KEY(`cup_id`)
 , CONSTRAINT `cup_customer_id` FOREIGN KEY (`customer_id`)
   REFERENCES `starbucks`.`customer` (`customer_id`)
);

CREATE TABLE IF NOT EXISTS `starbucks`.`receipt` (
   `receipt_id`            INT              NOT NULL AUTO_INCREMENT
 , `customer_id`           INT              NOT NULL
 , `store_id`              INT              NOT NULL
 , `timestamp_created`     TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP
 , INDEX(`receipt_id`)
 , INDEX(`timestamp_created`)
 , INDEX(`store_id`)
 , PRIMARY KEY(`receipt_id`)
 , CONSTRAINT `receipt_customer_id` FOREIGN KEY (`customer_id`)
   REFERENCES `starbucks`.`customer` (`customer_id`)
 , CONSTRAINT `receipt_store_id` FOREIGN KEY (`store_id`)
   REFERENCES `starbucks`.`store` (`store_id`)
);

CREATE TABLE IF NOT EXISTS `starbucks`.`purchase` (
   `purchase_id`           INT              NOT NULL AUTO_INCREMENT
 , `receipt_id`            INT              NOT NULL
 , `menu_item_id`          INT              NOT NULL
 , `cup_id`                INT              NULL
 , INDEX(`purchase_id`)
 , INDEX(`receipt_id`)
 , INDEX(`menu_item_id`)
 , INDEX(`cup_id`)
 , CONSTRAINT `purchase_receipt_id` FOREIGN KEY (`receipt_id`)
   REFERENCES `starbucks`.`receipt` (`receipt_id`)
 , CONSTRAINT `purchase_menu_item_id` FOREIGN KEY (`menu_item_id`)
   REFERENCES `starbucks`.`menu_item` (`menu_item_id`)
 , CONSTRAINT `purchase_cup_id` FOREIGN KEY (`cup_id`)
   REFERENCES `starbucks`.`cup` (`cup_id`)
);

CREATE TABLE IF NOT EXISTS `starbucks`.`wifi_presence` (
   `wifi_presence_id`      INT              NOT NULL AUTO_INCREMENT
 , `store_id`              INT              NOT NULL
 , `client_mac`            VARCHAR(32)      NOT NULL
 , `first_seen`            TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP
 , `last_seen`             TIMESTAMP        NULL
 , `converted`             BOOL             NOT NULL DEFAULT FALSE
 , INDEX(`wifi_presence_id`)
 , CONSTRAINT `wifi_presence_store_id` FOREIGN KEY (`store_id`)
   REFERENCES `starbucks`.`store` (`store_id`)
);

CREATE TABLE IF NOT EXISTS `starbucks`.`wifi_visit` (
   `wifi_visit_id`         INT              NOT NULL AUTO_INCREMENT
 , `wifi_presence_id`      INT              NULL
 , `store_id`              INT              NOT NULL
 , `customer_id`           INT              NOT NULL
 , `first_seen`            TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP
 , `last_seen`             TIMESTAMP        NULL
 , `wifi_login`            TIMESTAMP        NULL
 , `bytes_downloaded`      INT              NULL
 , `bytes_uploaded`        INT              NULL
 , INDEX(`wifi_visit_id`)
 , CONSTRAINT `wifi_visit_wifi_presence_id` FOREIGN KEY (`wifi_presence_id`)
   REFERENCES `starbucks`.`wifi_presence` (`wifi_presence_id`)
 , CONSTRAINT `wifi_visit_store_id` FOREIGN KEY (`store_id`)
   REFERENCES `starbucks`.`store` (`store_id`)
 , CONSTRAINT `wifi_visit_customer_id` FOREIGN KEY (`customer_id`)
   REFERENCES `starbucks`.`customer` (`customer_id`)
);

CREATE TABLE IF NOT EXISTS `starbucks`.`wifi_visit_receipt` (
   `wifi_visit_id`              INT              NOT NULL
 , `receipt_id`           INT              NOT NULL
 , CONSTRAINT `wifi_visit_purchase_wifi_visit_id` FOREIGN KEY (`wifi_visit_id`)
   REFERENCES `starbucks`.`wifi_visit` (`wifi_visit_id`)
 , CONSTRAINT `wifi_visit_purchase_receipt_id` FOREIGN KEY (`receipt_id`)
   REFERENCES `starbucks`.`receipt` (`receipt_id`)
);

INSERT `starbucks`.`store` SELECT * FROM `starbucks`.`store_backup`;
