import { createStackNavigator, createAppContainer } from 'react-navigation';

import HomeScreen from './components/HomeScreen';

import DrinksScreen from './components/DrinksScreen';
import DrinkOrderScreen from './components/DrinkOrderScreen';

const AppNavigator = createStackNavigator({
    Home: { screen: HomeScreen },
    Drinks: { screen: DrinksScreen },
    DrinkOrder: { screen: DrinkOrderScreen },
});

const AppNavigatorContainer = createAppContainer(AppNavigator);


export default AppNavigatorContainer;