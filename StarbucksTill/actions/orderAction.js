export const AddOrder = (order) => ({
    type: 'ADD_ORDER',
    payload: order
});

export const ClearOrder = () => ({
    type: 'CLEAR_ORDER'
});