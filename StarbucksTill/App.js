import React from 'react';
import { Provider } from 'react-redux';
import { combineReducers, createStore } from 'redux';

import orderReducer from './reducers/orderReducer';
import menuReducer from './reducers/menuReducer';

import AppNavigatorContainer from './AppNavigator';

const store = createStore(
  combineReducers({
    orders: orderReducer,
    menu: menuReducer
  })
);

export default class App extends React.Component {
  constructor(props) {
    super(props)
  };

  render() {
    return (
      <Provider store={ store }>
        <AppNavigatorContainer />
      </Provider>
    );
  };
}

