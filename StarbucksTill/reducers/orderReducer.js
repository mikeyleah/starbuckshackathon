const INITIAL_STATE = {
    orderList: [],
};

const orderReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'ADD_ORDER':
            const {
                orderList
            } = state;

            orderList.push(action.payload);

            return { orderList }
        case 'CLEAR_ORDER':
            return { orderList: [] };
        default:
            return state
    }
};

export default orderReducer;