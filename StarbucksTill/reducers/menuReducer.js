const INITIAL_STATE = {
    drinks: [
        {id: 1, name: 'Caffè Americano', cost: 1.75, description: 'Espresso shots are topped with hot water to produce a light layer of crema. The result is this wonderfully rich cup with depth and nuance.'},
        {id: 2, name: 'Caffè Latte', cost: 1.75, description: 'Our dark, rich espresso balanced with steamed milk and a light layer of foam. A perfect milk forward warm up.'},
        {id: 3, name: 'Caffè Mocha', cost: 1.75, description: 'We combine our rich, full-bodied espresso with bittersweet mocha sauce and steamed milk, then top it off with sweetened whipped cream. The classic coffee drink to satisfy your sweet tooth.'},
        {id: 4, name: 'Cappucino', cost: 1.75, description: 'Dark, rich espresso lies in wait under a smoothed and stretched layer of thick foam. It\'s truly the height of our baristas\' craft.'},
        {id: 5, name: 'Espresso', cost: 1.75, description: 'All of our handcrafted expresso drinks can be made with either shots of Starbucks Expresso Roast or Starbucks Blonde Expresso Roast.'},
        {id: 6, name: 'Flat White', cost: 1.75, description: 'Bold ristretto shots of espresso get the perfect amount of steamed whole milk to create a not too strong, not too creamy, just right flavor.'},
        {id: 7, name: 'Espresso Con Panna', cost: 1.75, description: 'Espresso meets a dollop of whipped cream to enhance the rich and caramelly flavors of a straight-up shot.'},
        {id: 8, name: 'Chai Latte', cost: 1.75, description: 'Black tea infused with cinnamon, clove, and other warming spices is combined with steamed milk and topped with foam for the perfect balance of sweet and spicy.'},
        {id: 9, name: 'Royal English Breakfast Tea', cost: 1.75, description: 'Each sip of this beloved morning black tea unfolds to reveal the complexity of the high grown full leaves. An elegant, time-honored classic that brings a royal nod to every cup.'},
        {id: 10, name: 'Teavana® Shaken Iced White Tea', cost: 1.75, description: 'Soft and delicate with a clean, bright finish, our white tea is shaken with ice for a crisp, subtly sweet, refined refreshment.'},
    ],
};

const menuReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        default:
            return state
    }
};

export default menuReducer;