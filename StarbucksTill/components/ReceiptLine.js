import React from 'react';
import { connect } from 'react-redux';
import { Text, ScrollView, View, StyleSheet } from 'react-native';

import { AddOrder } from '../actions/orderAction';
import { bindActionCreators } from 'redux';

class ReceiptLine extends React.Component {
    render() {
        return (
            <View style={styles.receiptLine}>
                <Text style={styles.drinkName}>{this.props.name}</Text>
                <View style={styles.drinkSize}>
                    <Text>Size</Text>
                    <Text>{this.props.size}</Text>
                </View>
            </View>    
        )
    };
}

const styles = StyleSheet.create({
    receiptLine: {
        marginBottom: 10
    },
    drinkName: {
        fontWeight: 'bold',
        fontSize: 16,
        borderBottomWidth: 1,
        borderColor: '#ccc',
        marginBottom: 3,
        paddingBottom: 3
    },
    drinkSize: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    }
});

export default ReceiptLine;