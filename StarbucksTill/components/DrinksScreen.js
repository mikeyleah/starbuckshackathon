import React from 'react';
import { Text, View, Button, StyleSheet, Image, TouchableHighlight, ScrollView } from 'react-native';
import { connect } from 'react-redux';

class DrinksScreen extends React.Component {
    static navigationOptions = {
        title: 'Drinks Menu'
    };

    render() {
        return (
            <View style={styles.container}>
                
                <View style={styles.header}>
                    <Image style={styles.headerLogo} source={require('../resources/images/starbucks.png')}/>
                </View>

                <View style={styles.subheaderContainer}>
                    <TouchableHighlight style={styles.button}>
                        <Text style={styles.buttonText}>Drinks</Text>
                    </TouchableHighlight>
                </View>

                <ScrollView>
                    <View style={styles.drinksList}>
                        {this.props.menu.drinks.map((drink, index) => {
                            return (
                                <View style={styles.drinkItem} key={index}>
                                    <Text 
                                        style={styles.circle} 
                                        onPress={() => {
                                                this.props.navigation.navigate('DrinkOrder', {id: drink.id, name: drink.name, cost: drink.cost, description: drink.description})
                                            }}>
                                    </Text>
                                    <TouchableHighlight
                                        onPress={() => {
                                            this.props.navigation.navigate('DrinkOrder', {id: drink.id, name: drink.name, cost: drink.cost, description: drink.description})
                                        }}
                                    >
                                        <Text style={styles.drinkItemText}>{drink.name}</Text>
                                    </TouchableHighlight>
                                </View>
                            )    
                        })}
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff'
    },
    header: {
        width: '100%',
        padding: 15,
        alignItems: 'center',
        backgroundColor: '#f7f7f7',
        elevation: 3
    },
    headerLogo: {
        margin: 10,
        width: 100,
        height: 100,
        alignItems: 'center'
    },
    drinksList: {
        backgroundColor: '#fff',
        width: '100%',
        margin: 25,
        height: 750
    },
    circle: {
        width: 100,
        height: 100,
        borderRadius: 25,
        backgroundColor: '#f7f7f7'
    },
    drinkItem: {
        flexDirection: 'row',
        marginBottom: 15
    },
    drinkItemText: {
        fontSize: 18,
        lineHeight: 37
    },
    circle: {
        width: 40,
        height: 40,
        borderRadius: 50,
        backgroundColor: '#c9cac9',
        marginRight: 10,
    },
    button: {
        backgroundColor: '#00704A',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 40,
        paddingRight: 40,
        borderRadius: 25,
        alignSelf: 'flex-start'
    },
    buttonText: {
        color: '#f7f7f7'
    },
    subheaderContainer: {
        width: '100%',
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: '#fff',
        elevation: 4
    }
})


const mapStateToProps = (state) => {
    const { menu } = state
    return { menu }    
};

export default connect(mapStateToProps)(DrinksScreen);