import React from 'react';
import { View, Text, Button, Image, StyleSheet, Alert, Picker, TouchableHighlight, ScrollView } from 'react-native';
import { connect } from 'react-redux';

import { AddOrder, ClearOrder } from './../actions/orderAction';
import { bindActionCreators } from 'redux';

import Receipt from './../components/Receipt';

class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Starbucks'
    };

    constructor(props) {
        super(props)
        this.state = {
            storeId: '1022778',
        };
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image style={styles.headerLogo} source={require('../resources/images/starbucks.png')}/>
                    <Text style={styles.headerText}>Welcome to Starbucks</Text>
                </View>
                
                <View style={styles.buttonGroup}>

                    <TouchableHighlight style={styles.button}>
                        <Text style={{color: '#f8f8f8'}}>Food</Text>
                    </TouchableHighlight>

                    <TouchableHighlight 
                        style={styles.button} 
                        onPress={() => {
                            this.props.navigation.navigate('Drinks', {})
                        }}
                    >
                        <Text style={{color: '#f8f8f8'}}>Drinks</Text>
                    </TouchableHighlight>
                </View>

                <ScrollView>
                    <View style={styles.orderContainer}>
                        <Text style={styles.orderTitle}>Your Order</Text>
                            <Receipt/>
                    </View>

                    <View>
                        <Picker
                            style={styles.storePicker}
                            selectedValue={this.state.storeId}
                            onValueChange={(itemValue, itemIndex) => {
                                this.setState({storeId: itemValue})
                            }}
                        >
                            <Picker.Item label="Murphys Corner" value="1022778"/>
                            <Picker.Item label="Paces Ferry" value="6366"/>
                            <Picker.Item label="42nd & 2nd" value="6869"/>
                            <Picker.Item label="Perimeter Mall" value="7816"/>
                            <Picker.Item label="Mission & Main" value="1021020"/>
                        </Picker>
                    </View>  
                </ScrollView>

                <View style={styles.purchaseContainer}>
                    <TouchableHighlight 
                        style={styles.purchaseButton}
                        onPress={() => {
                            let purchases = this.props.orders.orderList.map((order, index) => {
                                return {menuItemId: parseInt(order.id), cupId: order.cupId};
                            });

                            let customerId = parseInt(this.props.orders.orderList[0].customerId);
                            let storeId = parseInt(this.state.storeId);

                            if (!customerId > 0) {
                                customerId = 15;
                            }

                            const requestBody = {
                                customerId: customerId,
                                storeId: storeId,
                                purchases: purchases
                            };
                            
                            const requestData = {
                                method: 'PUT',
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-type': 'application/json',
                                },
                                body: JSON.stringify(requestBody)
                            };

                            Alert.alert('Processing purchase...');

                            fetch('http://13.68.220.82:80/order/', requestData)
                                .then(response => {
                                    if (response.status == 200) {
                                        this.props.ClearOrder();
                                    }
                                })
                                .catch((error) => {
                                    console.log(error);
                                });
                        }}
                    >
                        <Text style={styles.purchaseButtonText}>Place Order</Text>
                    </TouchableHighlight>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f7f7f7'
    },
    storePicker: { 
        backgroundColor: '#fff',
        margin: 15,
        borderWidth: 1,
        borderColor: '#000'
    }, 
    orderContainer: {
        backgroundColor: '#f7f7f7',
        padding: 15,
    },
    orderTitle: {
        fontSize: 16,
        fontWeight: 'bold'
    },

    purchaseContainer: {
        width: '100%',
        backgroundColor: '#fff',
        padding: 28,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.43,
        shadowRadius: 9.51,
        elevation: 15,
    },
    purchaseButton: {
        alignSelf: 'center',
        backgroundColor: '#00704A',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 40,
        paddingRight: 40,
        borderRadius: 25
    },
    purchaseButtonText: {
        color: '#f8f8f8'
    },
    buttonGroup: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-evenly',        
        padding: 25,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
        marginBottom: 15
    },
    button: {
        backgroundColor: '#00704A',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 40,
        paddingRight: 40,
        borderRadius: 25
    },
    header: {
        width: '100%',
        padding: 20,
        alignItems: 'center'
    },
    headerLogo: {
        margin: 10,
        width: 100,
        height: 100,
        alignItems: 'center'
    },
    headerText: {
        fontSize: 15
    }
});

const mapStateToProps = (state) => {
    const { orders } = state
    return { orders }    
};

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        AddOrder,
        ClearOrder
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);