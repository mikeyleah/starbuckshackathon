import React from 'react';
import { connect } from 'react-redux';
import { Text, ScrollView, View, StyleSheet } from 'react-native';

import { AddOrder } from '../actions/orderAction';
import { bindActionCreators } from 'redux';

import ReceiptLine from './ReceiptLine';

class Receipt extends React.Component {
    render() {
        return (
            <View style={styles.receiptContainer}>
                {this.props.orders.orderList.map((order, index) => {
                    return (
                        <ReceiptLine key={index} {...order}/>
                    )
                })}
            </View>
        )
    };
}

const styles = StyleSheet.create({
    receiptContainer: {
        margin: 10,
        backgroundColor: '#fff',
        padding: 25,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    }
});

const mapStateToProps = (state) => {
    const { orders } = state
    return { orders }    
};

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        AddOrder,
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Receipt);