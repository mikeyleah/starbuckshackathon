import React from 'react';
import { Platform, Text, View, Button, Picker, Image, ScrollView, StyleSheet, TouchableHighlight  } from 'react-native';
import { connect } from 'react-redux';
import NfcManager, {ByteParser, Ndef, NdefParser} from 'react-native-nfc-manager';

import { AddOrder } from '../actions/orderAction';
import { bindActionCreators } from 'redux';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff'
    },
    header: {
        width: '100%',
        padding: 10,
        alignItems: 'center',
        backgroundColor: '#f7f7f7',
    },
    headerLogo: {
        margin: 10,
        width: 100,
        height: 100,
        alignItems: 'center'
    },
    heroImage: {
        width: '100%',
        height: 175,
        marginBottom: 10
    },
    heroText: {
        marginTop: -40,
        marginLeft: 8,
        fontSize: 14,
        color: '#f7f7f7',
        fontWeight: 'bold'
        
    },
    buttonContainer: {
        backgroundColor: '#f7f7f7',
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 25,
    },
    button: {
        backgroundColor: '#00704A',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 40,
        paddingRight: 40,
        borderRadius: 25
    },
    buttonText: {
        color: '#f8f8f8'
    },
    costContainer: {
        backgroundColor: '#fff',
        padding: 16,
        elevation: 1,
    },
    costText: {
        fontSize: 15,
        alignSelf: 'flex-end',
        fontWeight: 'bold'
    },
    descriptionContainer: {
        marginTop: 8,
        padding: 15,
    },
    descriptionText: {
        fontSize: 16,
        marginBottom: 8
    },
    sizePicker: {
        backgroundColor: '#f5f5f5',
        marginBottom: 5
    },
    scroller: {
        height: '80%',
        overflow: 'hidden'
    }
});

class DrinkOrderScreen extends React.Component {
    static navigationOptions = {
        title: 'Add Drink to Order'
    };

    constructor(props) {
        super(props)
        this.state = {
            size: 'Venti',
            scanText: 'Scan Cup',
        
            customerID: '',
            cupID: '',
            
            // NFC State
            supported: true,
            enabled: false,
            isWriting: false,
            urlToWrite: 'https://www.google.com',
            parsedText: null,
            tag: {},

        };
    };

    componentDidMount() {
        NfcManager.isSupported().then(supported => {
            this.setState({ supported })
            if (supported) {
                this._startNfc();
            }
        });
    };

    componentWillUnmount() {
        this._stopDetection();

    };

    render() {
        const drinkID = this.props.navigation.getParam('id');
        const drinkName = this.props.navigation.getParam('name');
        const drinkCost = this.props.navigation.getParam('cost');
        const drinkDescription = this.props.navigation.getParam('description');
        let customerID = this.state.customerID;
        let cupID = this.state.cupID;
        let drinkSize = this.state.size;

        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image style={styles.headerLogo} source={require('../resources/images/starbucks.png')}/>
                </View>
                
                <ScrollView style={styles.scroller}>
                <View>
                    <Image style={styles.heroImage} source={require('../resources/images/flatwhite5.jpg')}/>
                    <Text style={styles.heroText}>{drinkName}</Text>
                </View>
                

                <View style={styles.descriptionContainer}>
                    <Text style={styles.descriptionText}>{drinkDescription}</Text>
                    <Picker
                        style={styles.sizePicker}
                        selectedValue={this.state.size}
                        onValueChange={(itemValue, itemIndex) => {
                            this.setState({size: itemValue})
                        }}
                    >
                        <Picker.Item style={styles.sizePickerItem} label="Short" value="Short"/>
                        <Picker.Item style={styles.sizePickerItem} label="Tall" value="Tall"/>
                        <Picker.Item style={styles.sizePickerItem} label="Grande" value="Grande"/>
                        <Picker.Item style={styles.sizePickerItem} label="Venti" value="Venti"/>
                    </Picker>
                </View>
                <View style={styles.costContainer}>
                    <Text style={styles.costText}>Total: ${drinkCost}</Text>
                </View>

                <View style={styles.buttonContainer}>
                    <TouchableHighlight 
                        style={styles.button}
                        onPress={() => {
                            this.setState({scanText: 'Scanning...'})
                            this._startDetection()
                        }}
                    >
                        <Text style={styles.buttonText}>{this.state.scanText}</Text>
                    </TouchableHighlight>

                    <TouchableHighlight 
                        style={styles.button}
                        onPress={() => {
                            this._stopDetection()
                            this.props.AddOrder(
                                {id: drinkID.toString(), name: drinkName.toString(), cost: drinkCost.toString(), size: drinkSize, cupId: cupID, customerId: customerID}
                            );
                            this.props.navigation.navigate('Home', {})
                        }}
                    >
                        <Text style={styles.buttonText}>Add Item</Text>
                    </TouchableHighlight>
                </View>

                </ScrollView>
            </View>
        )
    };

    // NFC Boilerplate
    _startNfc() {
        NfcManager.start({
            onSessionClosedIOS: () => {
                console.log('ios session closed');
            }
        })
        .then(result => {
            console.log('start OK', result);
        })
        .catch(error => {
            console.warn('start fail', error);
            this.setState({supported: false});
        })
    
        if (Platform.OS === 'android') {
            NfcManager.getLaunchTagEvent()
                .then(tag => {
                    console.log('launch tag', tag);
                    if (tag) {
                        this.setState({ tag });
                    }
                })
                .catch(err => {
                    console.log(err);
                })
            NfcManager.isEnabled()
                .then(enabled => {
                    this.setState({ enabled });
                })
                .catch(err => {
                    console.log(err);
                })
            NfcManager.onStateChanged(
                event => {
                    if (event.state === 'on') {
                        this.setState({enabled: true});
                    } else if (event.state === 'off') {
                        this.setState({enabled: false});
                    } else if (event.state === 'turning_on') {
                        // do whatever you want
                    } else if (event.state === 'turning_off') {
                        // do whatever you want
                    }
                }
            )
                .then(sub => {
                    this._stateChangedSubscription = sub; 
                    // remember to call this._stateChangedSubscription.remove()
                    // when you don't want to listen to this anymore
                })
                .catch(err => {
                    console.warn(err);
                })
        }
      }
    
    _startDetection = () => {
        NfcManager.registerTagEvent(this._onTagDiscovered)
            .then(result => {
                console.log('registerTagEvent OK', result)
            })
            .catch(error => {
                console.warn('registerTagEvent fail', error)
            })
    }
    
    _stopDetection = () => {
        NfcManager.unregisterTagEvent()
            .then(result => {
                console.log('unregisterTagEvent OK', result)
            })
            .catch(error => {
                console.warn('unregisterTagEvent fail', error)
            })
    }
    
    _onTagDiscovered = tag => {
        this.setState({ tag, scanText: tag.id });

        fetch('http://13.68.220.82:80/cup/'+ tag.id + '/')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({ cupID: responseJson.cupId, customerID: responseJson.customerId });
            })
            .catch((error) => {
                console.error(error);
            });
    }
}

const mapStateToProps = (state) => {
    const { order } = state
    return { order }    
};

const mapDispatchToProps = (dispatch) => (
    bindActionCreators({
        AddOrder,
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(DrinkOrderScreen);